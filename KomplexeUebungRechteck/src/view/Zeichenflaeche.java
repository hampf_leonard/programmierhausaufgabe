package view;
import java.awt.Color;
import model.Rechteck;
import java.awt.Graphics;
import controller.BunteRechteckeController;
import javax.swing.JPanel;


public class Zeichenflaeche extends JPanel {
//Attribute
	   protected BunteRechteckeController controller2;
//Konstruktor
	   public Zeichenflaeche(BunteRechteckeController controller2) {
	       this.controller2 = controller2;
	    }
	   public Zeichenflaeche() {
		   this.controller2 = controller2;
	   }
//Getter und Setter
	   public BunteRechteckeController getController2() {
	       return controller2;
	    }
	    public void setController2(BunteRechteckeController controller2) {
	        this.controller2 = controller2;
	    }
//Methoden
    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawRect(0,0, 50, 50);
    }
}