
public class Rechteck {
private int x;
private int y;
private int breite;
private int laenge;

public Rechteck() {
	this.x = 0;
	this.y = 0;
	this.breite = 0;
	this.laenge = 0;
	}
public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public int getBreite() {
	return breite;
}

public void setBreite(int breite) {
	this.breite = breite;
}

public int getLaenge() {
	return laenge;
}

public void setLaenge(int laenge) {
	this.laenge = laenge;
}

}
