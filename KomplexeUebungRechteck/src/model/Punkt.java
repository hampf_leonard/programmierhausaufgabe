package model;

//NaGseduZzLGZwSDBLhd5
public class Punkt {
	//Attribute
	private int x;
	private int y;
	//Konstruktor
	public Punkt() {
		this.x = 0;
		this.y = 0;
	}
	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}
	//setter und getter
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	//Method
	public boolean equals(Punkt p){
		return true;
	}
	public String toString() {
		return"X = "+getX()+", Y = "+getY()+".";
	}
}
