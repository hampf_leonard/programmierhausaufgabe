package model;
import model.Punkt;
//NaGseduZzLGZwSDBLhd5
public class Rechteck  {
	private Punkt p;
	private int breite;
	private int hoehe;

	public Rechteck(int x, int y, int breite, int hoehe) {
		this.p = new Punkt(x,y);
		this.breite = breite;
		this.hoehe = hoehe;
	}
	public Rechteck() {
		this.p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
		}
	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		this.p.setX(0);
	}

	public int getY() {
		return p.getX();
	}

	public void setY(int y) {
		this.p.setY(0);
	}

	public int getBreite() {
		if(breite < 0) {
			breite = breite + (-2*breite);
		}
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		if(hoehe < 0) {
			hoehe = hoehe + (-2*hoehe);
		}
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}
	//methoden
	public boolean enthaelt(int x, int y) {//1185,890
		boolean enthaelt = false;
		if(x == 1185 || x == 0) {
			enthaelt = true;
		}
		if(y == 890 || y == 0) {
			enthaelt = true;
		}
		return enthaelt;
	}
	public boolean enthaelt(Punkt p) {
		boolean enthaelt = false;
		if(p.getX() == 1185 || p.getX() == 0) {
			enthaelt = true;
		}
		if(p.getY() == 890 || p.getY() == 0) {
			enthaelt = true;
		}
		return enthaelt;
	}
	
	@Override //Es verbesert die Lesbarkeit des Codes
	//Wenn man eine �berschriebenen Methode �ndert, l�sen alle Unterklassen einen Kompilierungsfehler aus.
	//Bei einer gro�en Anzahl von Klassen, ist diese Anmerkung sehr hilfreich dabei, die betroffenen Klassen zu identifizieren.
	public String toString() {
		return "Rechteck[x="+p.getX()+", y="+p.getY()+", breite="+getBreite()+", hohe="+getHoehe()+"]";
	}

}

