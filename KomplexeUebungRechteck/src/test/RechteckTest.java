package test;

import model.Rechteck;
import model.Punkt;
import controller.BunteRechteckeController;

public class RechteckTest {
		public static void main(String[]args) {
			//programmierhausaufgabe 3
			Rechteck rechteck0 = new Rechteck(30,40,10,10);
			Rechteck rechteck1 = new Rechteck(25,25,100,20);
			Rechteck rechteck2 = new Rechteck(260,10,200,100);
			Rechteck rechteck3 = new Rechteck(5,500,300,25);
			Rechteck rechteck4 = new Rechteck(100,100,100,100);
			Rechteck rechteck5 = new Rechteck();
			rechteck5.setX(200);
			rechteck5.setY(200);
			rechteck5.setHoehe(200);
			rechteck5.setBreite(200);
			Rechteck rechteck6 = new Rechteck();
			rechteck6.setX(800);
			rechteck6.setY(400);
			rechteck6.setHoehe(20);
			rechteck6.setBreite(20);
			Rechteck rechteck7 = new Rechteck();
			rechteck7.setX(800);
			rechteck7.setY(450);
			rechteck7.setHoehe(20);
			rechteck7.setBreite(20);
			Rechteck rechteck8 = new Rechteck();
			rechteck8.setX(850);
			rechteck8.setY(400);
			rechteck8.setHoehe(20);
			rechteck8.setBreite(20);
			Rechteck rechteck9 = new Rechteck();
			rechteck9.setX(855);
			rechteck9.setY(455);
			rechteck9.setHoehe(25);
			rechteck9.setBreite(25);
			Rechteck rechteck10 = new Rechteck();
			//Programierhausaufgabe 4
			System.out.println(rechteck0.toString());
			System.out.println(rechteck0);
			System.out.println(rechteck0.equals(rechteck0.toString()));
			//Programmierhausaufgabe 5
			BunteRechteckeController controller = new BunteRechteckeController();
			controller.add(rechteck0);
			controller.add(rechteck1);
			controller.add(rechteck2);
			controller.add(rechteck3);
			controller.add(rechteck4);
			controller.add(rechteck5);
			controller.add(rechteck6);
			controller.add(rechteck7);
			controller.add(rechteck8);
			controller.add(rechteck9);
			System.out.println(controller.toString());
			controller.reset();
			System.out.println(controller.toString());
			controller.add(rechteck0);
			//Programmierhausaufgabe 8
			Punkt punkt6 = new Punkt(-4,5);
		    Rechteck eck10 = new Rechteck(-4,5,-50,-200);
		    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50,hoehe=200]
		    Rechteck eck11 = new Rechteck();
		    eck11.setX(-10);
		    eck11.setY(-10);
		    eck11.setBreite(-200);
		    eck11.setHoehe(-100);
		    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]

		}
	}
